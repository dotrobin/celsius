package test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;


public class CelsiusTest {
	
	@Test
	public void TestfromFahrenheitRegular() {
		int currentF = sheridan.Celsius.getFahrenheit();
		int celsius = sheridan.Celsius.fromFahrenheit(currentF);
		assertTrue("Invalid reading", celsius == 37);
	}
	
	
	@Test
	public void TestfromFahrenheitException() {
		int celsius = sheridan.Celsius.fromFahrenheit(-2);
		assertTrue("Invalid reading", celsius == -18);
	}
	

	@Test
	public void TestfromFahrenheitBoundaryIn() {
		int celsius = sheridan.Celsius.fromFahrenheit(32);
		
		assertTrue("Invalid reading", celsius == 0);
	}
	
	@Test
	public void TestfromFahrenheitBoundaryOut() {
		int celsius = sheridan.Celsius.fromFahrenheit(33);
		
		assertTrue("Invalid reading", celsius == 0);
	}
	

}
